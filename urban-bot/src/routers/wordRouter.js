const express = require('express');
const wordController = require('../controllers/wordController');

const router = new express.Router();

router.post('/words', wordController.show);

module.exports = router;
