const express = require('express');
const talkController = require('../controllers/talkController');

const router = new express.Router();

router.post('/talk', talkController.talk);

module.exports = router;
