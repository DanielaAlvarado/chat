const express = require('express');
const wordRouter = require('./routers/wordRouter');
const talkRouter = require('./routers/talkRouter');

const app = express();

app.use(express.json());

app.use(wordRouter);
app.use(talkRouter);

module.exports = app;
