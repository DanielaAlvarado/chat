const dialogflow = require('dialogflow');
const uuid = require('uuid');

exports.talk = async (req, res) => {
    try{
        const sessionId = uuid.v4();
        const sessionClient = new dialogflow.SessionsClient();
        const sessionPath = sessionClient.sessionPath(process.env.PROJECT_ID, sessionId);

        const request = {
            session: sessionPath,
            queryInput: {
                text: {
                    text: req.body.message,
                    languageCode: 'en-US',
                }
            }
        }

        const responses = await sessionClient.detectIntent(request);
        const result = responses[0].queryResult;
        
        if(!result.intent){
            return res.send({
                message: 'I don\'t understand'
            });
        }

        const message = result.fulfillmentText;
        res.send({
            message
        });
    }catch(error){
        res.status(500).send({
            message: 'I cannot answer right now'
        });
    }
};
