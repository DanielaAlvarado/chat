const fetch = require('node-fetch');

exports.show = async (req, res) => {
    const word = req.body.queryResult.parameters.word;
    const url = `http://api.urbandictionary.com/v0/define?term=${encodeURIComponent(word)}`;
    
    const response = await fetch(url);
    const definitions = await response.json();
    const definition = definitions.list[0].definition.replace(/\[/g, '').replace(/\]/g, '');

    res.send({
        fulfillmentText: definition,
        source: definitions.list[0].permalink
    });
};