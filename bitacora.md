# 27 de Mayo

Se alcanzó el objetivo para este sprint, esto es, agregar soporte Text-to-Speech.

Exploraron los frameworks siguientes:

- [Google Cloud Text-to-Speech](https://www.npmjs.com/package/@google-cloud/text-to-speech)
- [Google TTS](https://www.npmjs.com/package/google-tts-api), herramienta vinculada con Google Translate
- [SAY](https://www.npmjs.com/package/say)

Al final se quedaron con **SpeechSythesis** que es una extensión a HTML5 soportada por varios browsers modernos,
que incluyen a Firefox, Chrome y MS Edge, entre otros. Browsers viejos tales como Internet Explorer y Opera no lo
soportan.

---

Observaciones

El proyecto ha evolucionado positivamente en uno en el que se explora interación multimodal. La propuesta de 
extensión para el último sprint es de agregar "Speech recognition" y agregar botones para activar la interacción
con los bots. Opcionalmente, también permitir el cambiar los fonts y tamaño de botones para uso por personas
con capacidades distintas.

El documento de reporte puede hacer émfasis en el aspecto de la multi-modalidad.

# 3 de junio
Para ahora lograr Speech-to-text se usó la contraparte de SpeechSynthesis, llamada [SpeechRecognition](https://developer.mozilla.org/en-US/docs/Web/API/SpeechRecognition). La implementación fue sencilla, pero tiene la desventaja de que actualmente solo está soportada por Chrome y Edge.

Link del video al drive de la ultima reunion
https://drive.google.com/file/d/1xVqWltu6BYjZAhIwbwhSsC3DxC_TEkgv/view?usp=sharing