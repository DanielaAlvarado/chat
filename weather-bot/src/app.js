const express = require('express');
const weatherRouter = require('./routers/weatherRouter');
const talkRouter = require('./routers/talkRouter');

const app = express();

app.use(express.json());

app.use(weatherRouter);
app.use(talkRouter);

module.exports = app;
