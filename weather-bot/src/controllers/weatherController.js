const fetch = require('node-fetch');

exports.show = async (req, res) => {
    const location = req.body.queryResult.parameters.location;
    let url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(location)}.json?access_token=${process.env.MAPBOX_KEY}&limit=1`;
    
    let response = await fetch(url);
    response = await response.json();

    if(response.features.length == 0){
        return res.send({
            fulfillmentText: 'Location not found'
        });
    }
    const [longitude, latitude] = response.features[0].center;

    url = `https://api.darksky.net/forecast/${process.env.DARKSKY_KEY}/${latitude},${longitude}?units=si`
    response = await fetch(url);
    response = await response.json();
    const weather = `${response.daily.data[0].summary} It is currently ${response.currently.temperature} degrees. The high today is ${response.daily.data[0].temperatureHigh} with a low of ${response.daily.data[0].temperatureLow}. There is a ${response.currently.precipProbability * 100}% chance of rain.`

    res.send({
        fulfillmentText: weather
    });
};