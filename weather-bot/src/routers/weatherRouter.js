const express = require('express');
const weatherController = require('../controllers/weatherController');

const router = new express.Router();

router.post('/weather', weatherController.show);

module.exports = router;
