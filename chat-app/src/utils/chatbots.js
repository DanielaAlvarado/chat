const chatbots = [];

const addChatbot = ({name, room, endpoint}) => {
    name = name.trim().toLowerCase();
    room = room.trim().toLowerCase();

    if(!name || !room || !endpoint){
        return {
            error: 'Name, room and endpoint required'
        };
    }

    const existingChatbot = chatbots.find((chatbot) => {
        return chatbot.room === room && chatbot.name === name;
    });

    if(existingChatbot){
        return {
            error: 'Chatbot name in use'
        };
    }

    const chatbot = {
        name,
        room,
        endpoint
    };
    chatbots.push(chatbot);

    return {
        chatbot
    };
}

const getChatbot = (name, room) => {
    return chatbots.find((chatbot) => {
        return chatbot.name === name && chatbot.room === room;
    });
};

const getChatbotsInRoom = (room) => {
    room = room.trim().toLowerCase();

    return chatbots.filter((chatbot) => {
        return chatbot.room === room;
    });
};

module.exports = {
    addChatbot,
    getChatbot,
    getChatbotsInRoom
};