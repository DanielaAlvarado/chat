const generateMessage = (username, text, isBot = false) => {
    return {
        username,
        text,
        isBot,
        createdAt: new Date().getTime()
    };
};

module.exports = {
    generateMessage
}
