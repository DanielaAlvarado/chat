const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const fetch = require('node-fetch');
const {generateMessage} = require('./utils/messages');
const {addUser, removeUser, getUser, getUsersInRoom} = require('./utils/users');
const {addChatbot, getChatbot, getChatbotsInRoom} = require('./utils/chatbots');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

io.on('connection', (socket) => {
    socket.on('join', (options, callback) => {
        const {error, user} = addUser({
            id: socket.id,
            ...options
        });

        if(error){
            return callback(error);
        }

        socket.join(user.room)

        socket.emit('message', generateMessage('admin', 'Welcome.'));
        socket.broadcast.to(user.room).emit('message', generateMessage('admin', `${user.username} has joined`));

        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUsersInRoom(user.room),
            chatbots: getChatbotsInRoom(user.room)
        });

        callback();
    });

    socket.on('sendMessage', async (message, callback) => {
        const user = getUser(socket.id);
        io.to(user.room).emit('message', generateMessage(user.username, message));
        
        callback();

        if(message[0] == '#'){
            const chatbotName = message.split(' ')[0].replace('#', '');
            message = message.replace(`#${chatbotName}`, '');
            const chatbot = getChatbot(chatbotName, user.room);
            
            let response = await fetch(chatbot.endpoint, {
                method: 'post',
                body: JSON.stringify({
                    message
                }),
                headers: {
                    'Content-Type': 'application/json'    
                }
            });
            response = await response.json();
            const chatbotMessage = response.message;

            io.to(user.room).emit('message', generateMessage(chatbot.name, chatbotMessage, true));
        }
    });

    socket.on('disconnect', () => {
        const user = removeUser(socket.id)

        if(user){
            io.to(user.room).emit('message', generateMessage('admin', `${user.username} has left`));
            io.to(user.room).emit('roomData', {
                room: user.room,
                users: getUsersInRoom(user.room)
            });
        }
    });

    socket.on('addChatbot', (options, callback) => {
        const user = getUser(socket.id);
        const {error, chatbot} = addChatbot({
            ...options,
            room: user.room
        });

        if(error){
            return callback(error);
        }

        io.to(user.room).emit('message', generateMessage('admin', `${user.username} added chatbot ${chatbot.name}`));

        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUsersInRoom(user.room),
            chatbots: getChatbotsInRoom(user.room)
        });

        callback();
    });
});

const port = process.env.PORT || 3000;
server.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});
