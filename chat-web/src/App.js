import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Index from './components/Index';
import CreateRoom from './components/CreateRoom';
import JoinRoom from './components/JoinRoom';
import Chat from './components/Chat';

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/create-room">
                    <CreateRoom/>
                </Route>
                <Route path="/join-room">
                    <JoinRoom/>
                </Route>
                <Route path="/chat" render={(props) => {
                    return <Chat {...props}/>;
                }}></Route>
                <Route path="/">
                    <Index/>
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
