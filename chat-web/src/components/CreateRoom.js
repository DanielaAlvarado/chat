import React from 'react';
const shortid = require('shortid');

function CreateRoom(){
    return(
        <div className="centered-form">
            <div className="centered-form__box">
                <h3>Share this code with your friends</h3>
                <h1>{shortid.generate()}</h1>
            </div>
        </div>
    );
}

export default CreateRoom;