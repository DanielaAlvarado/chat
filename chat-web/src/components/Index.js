import React from 'react';
import {Link} from "react-router-dom";

function Index(){
    return(
        <div className="centered-form">
            <div className="centered-form__box">
                <Link to="/join-room"><button>Join chat room</button></Link>
                <Link to="/create-room"><button>Create chat room</button></Link>
            </div>
        </div>
    );
}

export default Index;