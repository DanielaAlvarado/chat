import React, {useState} from 'react';
import {Link} from "react-router-dom";

function JoinRoom(){
    const [username, setUsername] = useState('');
    const [room, setRoom] = useState('');

    return(
        <div className="centered-form">
            <div className="centered-form__box">
                <h1>Join room</h1>
                <form>
                    <label>Display name</label>
                    <input type="text" placeholder="Display name" required onChange={(event) => setUsername(event.target.value)}></input>
                    <label>Room</label>
                    <input type="text" name="room" placeholder="Room" required onChange={(event) => setRoom(event.target.value)}></input>
                    <Link to={{
                        pathname: '/chat',
                        state: {
                            username,
                            room
                        }
                    }}><button>Join</button></Link>
                </form>
            </div>
        </div>
    );
}

export default JoinRoom;