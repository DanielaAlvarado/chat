import React, {useEffect, useState, useRef} from 'react';
import io from 'socket.io-client';
import Moment from 'react-moment';
import Modal from 'react-modal';
import useStateWithCallback from 'use-state-with-callback';

Modal.setAppElement('#root')

function Chat(props){
    const {current: socket} = useRef(io('ws://localhost:3000/'));
    const {username, room} = props.location.state  || {
        username: '',
        room: ''
    };
    const synthesis = window.speechSynthesis;
    const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
    const recognition = new SpeechRecognition()
    recognition.continous = true;
    recognition.lang = 'en-US';

    const [messages, setMessages] = useState([]);
    const messagesRef = useRef([]);
    messagesRef.current = messages;
    
    const [users, setUsers] = useState([]);
    const [chatbots, setChatbots] = useState([]);
    const [modalIsOpen, setModalIsOpen] = useState(false);

    useEffect(() => {
        socket.emit('join', {
            username,
            room
        }, (error) => {
            if(error){
                alert(error);
                props.history.push('/');
            }
        });

        socket.on('message', async (message) => {
            setMessages([...messagesRef.current, message]);
            autoscroll();

            if(message.isBot){
                const utterance = new SpeechSynthesisUtterance(message.text);
                utterance.lang = 'en-US'
                synthesis.speak(utterance);
            }
        });
    
        socket.on('roomData', ({_, users, chatbots}) => {
            setUsers(users);
            setChatbots(chatbots);
        });

        return () => {
            socket.close();
        };
    }, []);

    const autoscroll = () => {
        const $messages = document.querySelector('#messages');
        const $newMessage = $messages.lastElementChild;

        const newMessageStyles = getComputedStyle($newMessage);
        const newMessageMargin = parseInt(newMessageStyles.marginBottom);
        const newMessageHeight = $newMessage.offsetHeight + newMessageMargin;

        const visibleHeight = $messages.offsetHeight;
        const containerHeight = $messages.scrollHeight;

        const scrollOffset = $messages.scrollTop + visibleHeight;

        if(containerHeight - newMessageHeight <= scrollOffset){
            $messages.scrollTop = $messages.scrollHeight;
        }
    }

    const sendMessage = (event) => {
        event.preventDefault();
        
        const $messageFormButton = document.querySelector('#send-message');
        $messageFormButton.setAttribute('disabled', 'disabled');

        const $messageFormInput = document.querySelector('#message');
        const message = $messageFormInput.value;

        socket.emit('sendMessage', message, (error) => {
            $messageFormButton.removeAttribute('disabled');
            $messageFormInput.value = '';
            $messageFormInput.focus();

            if(error){
                return alert(error);
            }
        });
    };

    const modalStyles = {
        content : {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            border: 'none'
        }
    };

    const openModal = () => {
        setModalIsOpen(true);
    };

    const closeModal = () =>{
        setModalIsOpen(false);
    };

    const addChatbot = (event) => {
        event.preventDefault();
        
        const $addChatbotFormButton = document.querySelector('#add-chatbot');
        $addChatbotFormButton.setAttribute('disabled', 'disabled');

        const $chatBotNameFormInput = document.querySelector('#chatbot-name');
        const chatbotName = $chatBotNameFormInput.value;
        const $chatBotEndpointFormInput = document.querySelector('#chatbot-endpoint');
        const chatbotEndpoint = $chatBotEndpointFormInput.value;

        socket.emit('addChatbot', {
            name: chatbotName,
            endpoint: chatbotEndpoint
        }, (error) => {
            $addChatbotFormButton.removeAttribute('disabled');

            $chatBotNameFormInput.value = '';
            $chatBotEndpointFormInput.value = '';

            if(error){
                return alert(error);
            }

            closeModal();
        });
    };

    const listen = () => {
        recognition.start();

        let finalTranscript = '';
        recognition.onresult = (event) => {
            for(let i = event.resultIndex; i < event.results.length; i++){
                const transcript = event.results[i][0].transcript;

                if(event.results[i].isFinal){
                    finalTranscript += transcript + ' ';
                }
            }

            const $messageFormInput = document.querySelector('#message');
            const message = $messageFormInput.value;
            document.querySelector('#message').value = `${message} ${finalTranscript}`;
        }
    };

    const writeChatbotName = (name) => {
        document.querySelector('#message').value = `#${name}`;
    }


    return(
        <div className="chat">
            <div className="chat__sidebar">
                <h2 className="list-title">Users</h2>
                <ul className="users">
                    {users.map((user) => 
                        <li>{user.username}</li>
                    )}
                </ul>
                <h2 className="list-title">Chatbots</h2>
                <ul className="users">
                    {chatbots.map((chatbot) => 
                        <li onClick={() => {writeChatbotName(chatbot.name)}}>{chatbot.name}</li>
                    )}
                </ul>
            </div>
            <div className="chat__main">
                <div className="chat__messages" id="messages">
                    {messages.map((message) => 
                        <div className="message">
                            <p>
                                <span className="message__name">{message.username}</span>
                                <span className="message__meta"><Moment format="h:mm a">{message.createdAt}</Moment></span>
                            </p>
                            <p>{message.text}</p>
                        </div>
                    )}
                </div>
                <div className="compose">
                    <button onClick={listen}>Listen</button>
                    <form onSubmit={(event) => sendMessage(event)}>
                        <input type="text" id="message" required autoComplete="off"></input>
                        <button id="send-message">Send</button>
                    </form>
                    <button onClick={openModal}>Add chatbot</button>
                </div>
            </div>
            <Modal isOpen={modalIsOpen} onRequestClose={closeModal} contentLabel="Add chatbot" style={modalStyles}>
                <div className="centered-form__box">
                    <h2>Add chatbot to this room</h2>
                    <form onSubmit={addChatbot}>
                        <label>Name</label>
                        <input type="text" placeholder="Name" required id="chatbot-name"></input>
                        <label>Endpoint</label>
                        <input type="text" placeholder="Endpoint" required id="chatbot-endpoint"></input>
                        <div className="centered-form__buttons">
                            <button onClick={closeModal}>Cancel</button>
                            <button id="add-chatbot">Add</button>
                        </div>
                    </form>
                </div>
            </Modal>
        </div>
    );
}

export default Chat;