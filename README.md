# Chat with bot integration
*Everardo Becerril Delgado A01552016*

*Daniela Alvarado Pereda A01329233*

## Description
This project implements a general purpose chat with generated rooms that can hold multiple users. The users can communicate through a standard chat interface. Additionally, any user can add chatbots to the room, these chatbots can communicate with users through text-to-speech and speech-through-text.

## Elements
### Architecture
The backend uses Node and runs a standard Express app. The file `src/index.js` sets up the app and implements the necessary functionalities to make the chat work. The WebSocket protocol is used through the [socket.io](https://www.npmjs.com/package/socket.io) package (it is not a WebSocket implementation, but it uses WebSocket as a transport). The folder `src/utils` contains some helper code to generate messages and manage chatbots and users.

The frontend is a React app with a few different routes, one allows the user to generate code for a room (using the [shortid](https://www.npmjs.com/package/shortid) package), another one allows the user to join a chat room. Finally, when a user joins a chat room, they are presented with a chat interface. The [socket.io-client](https://www.npmjs.com/package/socket.io-client) package is used to interact with the backend.

A couple of sample chatbots were developed using [Dialogflow](https://dialogflow.com/) with a custom fulfillment webhook. Then, an endpoint was developed in order to communicate with the bot via HTTP requests and messages in JSON format, this was achieved with the [dialogflow](https://www.npmjs.com/package/dialogflow) package. Both the webhook and the endpoint were implemented using Express.

### External services
Some external services were used to develop the sample chatbots. For `weather-bot` (a bot that tells you about the weather in a specific location), the [Mapbox Geocoding API](https://docs.mapbox.com/api/search/#geocoding) along with the [Dark Sky API](https://darksky.net/dev) where used in order to obtain the coordinates of the location given by the user and then get the weather conditions in that location.

For `urban-bot`, a bot that retrieves word definitions from Urban Dictionary, the Urban Dictionary API was used. Both `weather-bot` and `urban-bot` use the [node-fetch](https://www.npmjs.com/package/node-fetch) package to send HTTP request to the corresponding external API.

### Database
For this project we decided not to use a database. This was decided because we are constantly working in projects focused in retrieving and storing information in a database and we wanted to try something different and more dynamic.

Additionally, the lack of database operations allowed us to focus on other aspects of the project, like the integration of chatbots, and text-to-speech and speech-to-text functionalities.

### Asynchronous and synchronous interactions
All of the chat functionalities are synchronous, like sending and receiving messages, joining or leaving a chatroom and adding a chatbot.

The asynchronous operations begin when sending a message to a chatbot, when this happens, the chat is not blocked until the chatbot responds. Instead, an asynchronous request is sent to the chatbot's endpoint and its reply is sent back as a chat message once it responds.

At the same time, asynchronous requests are made to external APIs inside each chatbot's webhook. Finally, the interaction with the Dialogflow API is asynchronous.

### Emerging interaction elements
Chatbots are one of the emerging interaction elements used for this project. The chat attempts to be of general purpose, meaning that you can add any chatbot to it, and a chatbot can do anything that can be done by a Node app. This makes the integration very powerful.

The other elements are text-to-speech and speech-to-text. To implement these features it was necessary to research the multiple options available for each one. For text-to-speech, the following alternatives were explored.

* [Google Cloud Text-to-Speech](https://www.npmjs.com/package/@google-cloud/text-to-speech): this option was rejected because it was necessary to enable billing for the project.
* [google-tts-api](https://www.npmjs.com/package/google-tts-api): this alternative uses Google Translate to generate speech based on some text. This option was rejected because of the CORS issues it caused.
* [react-say](https://www.npmjs.com/package/react-say): this option is based on SpeechSynthesis, it was rejected because it was deemed harder to use than SpeechSynthesis.
* [SpeechSynthesis](https://developer.mozilla.org/en-US/docs/Web/API/SpeechSynthesis): this was the option we ended up using, it is easy to implement and it is supported natively by most modern browsers.

For speech-to-text we explored multiple options but all of them were based on [SpeechRecognition](https://developer.mozilla.org/en-US/docs/Web/API/SpeechRecognition) (the counterpart to SpeechSynthesis), so we decided to use SpeechRecognition directly. It has the downside that it is not currently supported by most browsers (it is only supported by Chrome and Edge).

## Documentation
### Repository and Trello
The code for the chat and both sample chatbots can be found in [this](https://bitbucket.org/DanielaAlvarado/chat) Bitbucket repository. The repository also includes and integration with a Trello board

### Videos
https://drive.google.com/file/d/1xVqWltu6BYjZAhIwbwhSsC3DxC_TEkgv/view?usp=sharing

## Conclusion
### Everardo
At the beggining it was a little difficult to define the scope of the project, but while we were developing the application we noticed how we could improve it by adding some special functionalities such as text-to-speech or speech-to-text that makes our project different from other kinds of similar applications. I have never used this type of technology and it was interesting its implementation works, due to the fact that in previous projects or in most of our projects we work assuming that users should be normal users and we didn't think about users with some kind of disability. In conclussion it was a great project and I enjoyed it a lot.

### Daniela
For me, this project was an introduction to a different kind of web applications, one that is not as concerned with data and is more focused on usability and user interaction. This kind of dynamic web application allows for easier integration of technology into everyday life. Functionalities like text-to-speech and speech-to-text can be encountred frequently and it was very interesting to find out how they can be implemented. This project gave us a preview into how the applications we use more frequently nowadays actually work behind the scenes.